package com.fleet.telemetry.legacy.repository.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.fleet.telemetry.legacy.exception.FleetTelemetryException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public abstract class FleetTelemetryS3Repository implements S3Repository<TrackKey, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FleetTelemetryS3Repository.class);

    private final AmazonS3 s3;
    private final String bucketName;

    public FleetTelemetryS3Repository(final AmazonS3 s3, final String bucketName) {
        this.s3 = s3;
        this.bucketName = bucketName;
    }

    @Override
    public final void save(final TrackKey trackKey, final String tracks) {
        Assert.notNull(trackKey, "TrackKey should not be null");
        Assert.hasText(tracks, "tracks should not be null or empty");
        LOGGER.debug("Saving tracks with key name: {}", trackKey.getKeyName());
        String filename = getFilename(trackKey);
        ObjectMetadata metadata = createObjectMetadata(trackKey, tracks);
        try (InputStream inputStream = new ByteArrayInputStream(tracks.getBytes())) {
            s3.putObject(bucketName, filename, inputStream, metadata);
        } catch (AmazonServiceException e) {
            LOGGER.error("Cannot save the tracks on S3, key: {}", trackKey.getKeyName());
            throw new FleetTelemetryException(e.getMessage());
        } catch (IOException e) {
            LOGGER.error("Cannot handle the input stream");
            throw new FleetTelemetryException(e.getMessage());
        }
    }

    protected abstract String getFilename(final TrackKey trackKey);

    private ObjectMetadata createObjectMetadata(final TrackKey trackKey, final String tracksJson) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(tracksJson.getBytes().length);
        metadata.setContentType("application/json");
        metadata.addUserMetadata("x-amz-meta-title", trackKey.getKeyName());
        return metadata;
    }

    @Override
    public final String findByKey(final TrackKey trackKey) {
        Assert.notNull(trackKey, "TrackKey should not be null");
        try {
            String filename = getFilename(trackKey);
            S3Object s3Object = s3.getObject(bucketName, filename);
            S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
            return IOUtils.toString(objectInputStream, String.valueOf(StandardCharsets.UTF_8));
        } catch (AmazonServiceException | IOException e) {
            LOGGER.error("Cannot find/read a S3 file, key: {}", trackKey.getKeyName());
            throw new FleetTelemetryException(e.getMessage());
        }
    }

    @Override
    public final boolean doesObjectExists(final TrackKey trackKey) {
        Assert.notNull(trackKey, "TrackKey should not be null");
        try {
            String filename = getFilename(trackKey);
            return s3.doesObjectExist(bucketName, filename);
        } catch (AmazonServiceException e) {
            LOGGER.error("Cannot see if the object exists, key: {}", trackKey.getKeyName());
            throw new FleetTelemetryException(e.getMessage());
        }
    }

}
