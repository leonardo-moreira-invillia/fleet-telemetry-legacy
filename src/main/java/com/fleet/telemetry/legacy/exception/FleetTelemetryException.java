package com.fleet.telemetry.legacy.exception;

public class FleetTelemetryException extends RuntimeException {

    public FleetTelemetryException(String message) {
        super(message);
    }

    public FleetTelemetryException(final Exception e) {
        super(e);
    }
}
