package com.fleet.telemetry.legacy.config;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("localstack")
public class AwsLocalStackConfig {

    @Value("${cloud.aws.region.static}")
    private String amazonRegion;

    @Value("${fleet.telemetry.order.s3.endpoint}")
    private String amazonS3Endpoint;

    @Bean
    @Primary
    public AmazonS3 s3() {
        return AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(amazonS3Endpoint, amazonRegion))
                .withPathStyleAccessEnabled(true)
                .build();
    }

}
