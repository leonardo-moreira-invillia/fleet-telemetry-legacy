package com.fleet.telemetry.legacy.config;

import com.fleet.telemetry.legacy.model.Tenant;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

@Configuration
@ComponentScan("com.fleet.telemetry.legacy")
@EnableConfigurationProperties
public class AppConfig {

    @Bean("brDataSource")
    @ConfigurationProperties(prefix="spring.brdatasource")
    public DataSource brDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("mxDataSource")
    @ConfigurationProperties(prefix="spring.mxdatasource")
    public DataSource secondaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("brJdbcTemplate")
    public JdbcTemplate brJdbcTemplate(DataSource brDataSource) {
        return new JdbcTemplate(brDataSource);
    }

    @Bean("mxJdbcTemplate")
    public JdbcTemplate mxJdbcTemplate(DataSource mxDataSource) {
        return new JdbcTemplate(mxDataSource);
    }

    @Bean("templateMap")
    public Map<Tenant, JdbcTemplate> jdbcTemplateMap(JdbcTemplate brJdbcTemplate, JdbcTemplate mxJdbcTemplate) {
        Map<Tenant, JdbcTemplate> jdbcTemplateMap = new HashMap<>();
        jdbcTemplateMap.put(Tenant.BR, brJdbcTemplate);
        jdbcTemplateMap.put(Tenant.MX, mxJdbcTemplate);
        return jdbcTemplateMap;
    }

}
