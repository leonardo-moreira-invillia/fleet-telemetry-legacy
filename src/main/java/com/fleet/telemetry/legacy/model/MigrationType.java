package com.fleet.telemetry.legacy.model;

public enum MigrationType {
    ORDER, ROUTE
}
