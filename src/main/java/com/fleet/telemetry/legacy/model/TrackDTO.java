package com.fleet.telemetry.legacy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrackDTO {

    private String trackDate;
    private Double latitude;
    private Double longitude;

}
