package com.fleet.telemetry.legacy.dao;

import com.fleet.telemetry.legacy.model.Tenant;
import com.fleet.telemetry.legacy.model.TrackDTO;

import java.util.List;
import java.util.Map;

public interface RouteTrackDao {

    List<TrackDTO> getByRouteId(Tenant tenant, Long routeId);

    TrackDTO createFromMap(Map<String, Object> map);

}
