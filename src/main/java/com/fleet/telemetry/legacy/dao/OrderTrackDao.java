package com.fleet.telemetry.legacy.dao;

import com.fleet.telemetry.legacy.model.Tenant;
import com.fleet.telemetry.legacy.model.TrackDTO;

import java.util.List;
import java.util.Map;

public interface OrderTrackDao {

    List<TrackDTO> getByOrderId(Tenant tenant, Long orderId);

    TrackDTO createFromMap(Map<String, Object> map);

}
