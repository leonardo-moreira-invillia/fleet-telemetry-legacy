package com.fleet.telemetry.legacy.dao;

import com.fleet.telemetry.legacy.model.Tenant;
import com.fleet.telemetry.legacy.model.TrackDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class RouteTrackDaoImpl implements RouteTrackDao {

    @Autowired
    Map<Tenant, JdbcTemplate> templateMap;

    private final String query =
        "select rt.id, tr.track_date, tr.latitude, tr.longitude from route rt inner join track tr on rt.trip_id = tr.trip_id where rt.id = ?";

    @Override
    public List<TrackDTO> getByRouteId(Tenant tenant, Long routeId) {
        List<Map<String, Object>> rows = templateMap.get(tenant).queryForList(query, new Object[] {routeId});
        List<TrackDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public TrackDTO createFromMap(Map<String, Object> map) {
        TrackDTO dto = new TrackDTO();
        dto.setTrackDate(map.get("track_date").toString());
        dto.setLatitude((Double) map.get("latitude"));
        dto.setLongitude((Double) map.get("longitude"));
        return dto;
    }

}
