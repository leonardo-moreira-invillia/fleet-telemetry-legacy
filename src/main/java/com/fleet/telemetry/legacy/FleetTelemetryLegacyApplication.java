package com.fleet.telemetry.legacy;

import com.fleet.telemetry.legacy.service.FileConsolidation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FleetTelemetryLegacyApplication {

    public static void main(String[] args) {
        SpringApplication.run(FleetTelemetryLegacyApplication.class, args);
    }

    @Autowired
    FileConsolidation fileConsolidation;

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            fileConsolidation.initialize();
        };
    }
}
