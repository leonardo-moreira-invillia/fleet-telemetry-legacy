package com.fleet.telemetry.legacy.utils;

import com.fleet.telemetry.legacy.model.Tenant;

public class TelemetryUtils {

    private TelemetryUtils() {
    }

    public static String createIdTenant(final String id, final String tenant) {
        return String.format("%s-%s", id, tenant.toLowerCase());
    }

    public static String createIdTenant(final Long id, final String tenant) {
        return createIdTenant(id.toString(), tenant);
    }

    public static String createIdTenant(final Long id, final Tenant tenant) {
        return createIdTenant(id.toString(), tenant.name());
    }

}
