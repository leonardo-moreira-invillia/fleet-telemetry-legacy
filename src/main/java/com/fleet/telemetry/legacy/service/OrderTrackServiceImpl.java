package com.fleet.telemetry.legacy.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fleet.telemetry.legacy.dao.OrderTrackDao;
import com.fleet.telemetry.legacy.model.Tenant;
import com.fleet.telemetry.legacy.model.TrackDTO;
import com.fleet.telemetry.legacy.repository.s3.FleetTelemetryS3Repository;
import com.fleet.telemetry.legacy.repository.s3.TrackKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Service("orderTrackService")
@Slf4j
public class OrderTrackServiceImpl implements TrackService {

    @Autowired
    private OrderTrackDao orderTrackDao;

    @Autowired
    @Qualifier("orderS3Repository")
    private FleetTelemetryS3Repository orderS3Repository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<TrackDTO> getByTenantId(Tenant tenant, Long orderId) {
        return orderTrackDao.getByOrderId(tenant, orderId);
    }

    @Override
    public void consolidate(Tenant tenant, Long id, final List<TrackDTO> tracks) {
        try {
            orderS3Repository.save(new TrackKey(id, tenant.name()), objectMapper.writeValueAsString(tracks));
        } catch (JsonProcessingException e) {
            log.error("An error occurred while serializing order data to JSON.", e);
        }
    }
}
