package com.fleet.telemetry.legacy.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fleet.telemetry.legacy.dao.RouteTrackDao;
import com.fleet.telemetry.legacy.model.Tenant;
import com.fleet.telemetry.legacy.model.TrackDTO;
import com.fleet.telemetry.legacy.repository.s3.FleetTelemetryS3Repository;
import com.fleet.telemetry.legacy.repository.s3.TrackKey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Service("routeTrackService")
@Slf4j
public class RouteTrackServiceImpl implements TrackService {

    @Autowired
    private RouteTrackDao routeTrackDao;

    @Autowired
    @Qualifier("routeS3Repository")
    private FleetTelemetryS3Repository routeS3Repository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<TrackDTO> getByTenantId(Tenant tenant, Long routeId) {
        return routeTrackDao.getByRouteId(tenant, routeId);
    }

    @Override
    public void consolidate(final Tenant tenant, final Long id, final List<TrackDTO> tracks) {
        try {
            routeS3Repository.save(new TrackKey(id, tenant.name()), objectMapper.writeValueAsString(tracks));
        } catch (JsonProcessingException e) {
            log.error("An error occurred while serializing route data to JSON.", e);
        }
    }
}
