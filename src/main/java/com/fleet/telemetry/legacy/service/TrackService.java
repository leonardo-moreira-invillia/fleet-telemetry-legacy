package com.fleet.telemetry.legacy.service;

import com.fleet.telemetry.legacy.model.Tenant;
import com.fleet.telemetry.legacy.model.TrackDTO;

import java.util.List;

public interface TrackService {

    List<TrackDTO> getByTenantId(Tenant tenant, Long orderId);

    void consolidate(Tenant tenant, Long id, final List<TrackDTO> tracks);

}
