package com.fleet.telemetry.legacy.service;

import static com.fleet.telemetry.legacy.model.MigrationType.ORDER;
import static com.fleet.telemetry.legacy.utils.TelemetryUtils.createIdTenant;

import com.fleet.telemetry.legacy.model.MigrationType;
import com.fleet.telemetry.legacy.model.Tenant;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class FileConsolidationImpl implements FileConsolidation {

    @Value("${filename:sourcedata.txt}")
    private Resource dataResource;

    @Value("${tenant:BR}")
    private Tenant tenant;

    @Value("${type:ORDER}")
    private MigrationType type;

    @Autowired
    @Qualifier("orderTrackService")
    private TrackService orderTrackService;

    @Autowired
    @Qualifier("routeTrackService")
    private TrackService routeTrackService;

    @Override
    public void initialize() {
        log.info("Consolidating {} of tenant {} from {} file.", type, tenant, dataResource.getFilename());

        TrackService trackService = ORDER.equals(type) ? orderTrackService : routeTrackService;

        try (Stream<String> dataStream = Files.lines(Paths.get(Objects.requireNonNull(dataResource.getFilename())))) {
            dataStream.filter(StringUtils::isNoneBlank)
                    .map(Long::valueOf)
                    .map(id -> Pair.of(id, trackService.getByTenantId(tenant, id)))
                    .peek(idTracks -> log.debug("Fetched {} tracks of {} {}", idTracks.getValue().size(), type, createIdTenant(idTracks.getKey(), tenant)))
                    .filter(idTracks -> !idTracks.getValue().isEmpty())
                    .forEach(idTracks -> trackService.consolidate(tenant, idTracks.getKey(), idTracks.getValue()));
        } catch (IOException e) {
            log.error("An error occurred while reading data file.", e);
        }
    }
}
