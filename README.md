## Fleet Telemetry Legacy Consolidation

This project intents to consolidate legacy telemetry data from Fleet's PostgreSQL database to AWS S3.

To achieve this goal, this Spring Boot application reads an data file containing one number per line, which represents an order or a route id.
For each id, its telemetry information are retrieved from Fleet's Database,
processed and consolidated on AWS S3 bucket (`development-ifood-fleet-worker-telemetry` on DEV environment).

Three parameters are expected to configure the application execution, as listed below with its default values:

1. filename: The name of the file that contains one id per line. (default: "sourcedata.txt");
2. tenant: An valid tenant in uppercase. Possible values are BR and MX. (default: BR);
3. type: The type of migration. Possible values are ORDER and ROUTE. (default: ORDER).

### Compiling

To compile the project, run `mvn clean package`.

### Database Connection

This project must connect on the two Fleet's databases for BR and MX telemetry consolidation.
Therefore, connection properties must be set on `application.properties` file as following:

```
spring.brdatasource.jdbcUrl=jdbc:postgresql://dev-pg-logistics1.dc.ifood.com.br:5432/ifood-fleet
spring.brdatasource.username=myusername
spring.brdatasource.password=mypassword

spring.mxdatasource.jdbcUrl=jdbc:postgresql://dev-pg-logistics1.dc.ifood.com.br:5432/ifood-fleet-mx
spring.mxdatasource.username=myusername
spring.mxdatasource.password=mypassword
```

### Running the application

The application can be started as any Spring Boot app: `java -jar package.jar` ou `mvn spring-boot:run`.
As explained above, running it without parameters is the same as calling the following command

```
java -jar package.jar --filename=sourcedata.txt --tenant=BR --type=ORDER
```
or 
```
mvn spring-boot:run -Dfilename=sourcedata.txt -Dtenant=BR -Dtype=ORDER
```

For route consolidation of MX tenant from an file called `routedata.txt`, for example, execute:

```
java -jar package.jar --filename=routedata.txt --tenant=MX --type=ROUTE
```
or
```
mvn spring-boot:run -Dfilename=routedata.txt -Dtenant=MX -Dtype=ROUTE
```

### Running locally

To run this application locally, it's necessary to start Localstack for consolidating on a bucket.
Vide https://bitbucket.org/ifood/ifood-delivery-platform-local-environment.

Then, the application can be executed by calling `mvn spring-boot:run -Dspring.profiles.active=localstack`.